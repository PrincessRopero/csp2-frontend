// // window.location.search returns the query string part of the URL
// console.log(window.location.search)

// instantiate a URLSearchParams object so we can access specific parts of the query string
let params = new URLSearchParams(window.location.search)

// get returns the value of the key as passed as an argument, then we store it in a variable
let token = localStorage.getItem("token")
let adminUser = localStorage.getItem("isAdmin")
let userLoginToken = localStorage.getItem("id")

let courseId = params.get('courseId')

let courseName = document.querySelector("#courseName");
let courseDesc = document.querySelector("#courseDesc");
let coursePrice = document.querySelector("#coursePrice");
let enrollContainer = document.querySelector("#enrollContainer");
let reviewDetails = document.querySelector("#reviewDetails")

if(adminUser === "true" || adminUser === true){
	fetch(`https://cess-csp2.herokuapp.com/api/courses/${courseId}`)
	.then(res => res.json())
	.then(data => {
		// console.log(data)
			courseName.innerHTML = data.name;
			courseDesc.innerHTML = data.description;
			coursePrice.innerHTML = data.price;

			enrolleesContainer.innerHTML = 
			`
				<table class="table">
					<thead>
						<h3 class="mt-5">Enrollees</h3>
						<tr>
							<th>First Name</th>
							<th>Last Name</th>
						</tr>
					</thead>
					<tbody id="enrolleees"></tbody>
				</table>
			`
			let enrolleees = document.querySelector("#enrolleees");
		data.enrollees.map(user => {

			fetch(`https://cess-csp2.herokuapp.com/api/users/${user.userId}`, {
				headers:{
					'Authorization': `Bearer ${token}`
				}
			})
			.then(res => res.json())
			.then(userinfo => {
				// console.log(userinfo)
				enrolleees.innerHTML +=
					`
					<tr>
						<td>${userinfo.firstName}</td>
                        <td>${userinfo.lastName}</td>
                    </tr>
					`
			
			})
		})
	})
}

else{

	fetch(`http://localhost:3000/api/courses/${courseId}`)
	.then(res => res.json())
	.then(data => {
		let userList = []
		courseName.innerHTML = data.name;
		courseDesc.innerHTML = data.description;
		coursePrice.innerHTML = data.price;
			data.enrollees.forEach(users => {
				userList.push(users.userId)

			})

				if(!userList.includes(userLoginToken)) {
					enrollContainer.innerHTML = `<button id="enrollButton" class="btn btn-block btn-primary">Enroll</button>`
				
				document.querySelector("#enrollButton").addEventListener("click", () => {
					fetch(`http://localhost:3000/api/users/enroll`, {
						method: "PUT",
						headers: {
							"Content-Type": "application/json",
							'Authorization': `Bearer ${token}`
						},
						body: JSON.stringify({
							courseId: courseId,
						})
					})
					.then((res) => {
						return res.json()
					})
					.then((data) => {

									if(data === true){
										alert("Thank you for enrolling!")
										window.location.replace("./courses.html")
									}
					})
				})

			} else{
				enrollContainer.innerHTML = null;
			}
	})
}