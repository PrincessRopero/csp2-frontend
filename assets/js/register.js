let registerForm = document.querySelector('#registerUser')

registerForm.addEventListener("submit", (e) => {
	e.preventDefault() //para maprevent yung default behavior na nagloload ang page pag nag click submit

	let firstName = document.querySelector("#firstName").value
	let lastName = document.querySelector("#lastName").value
	let mobileNo = document.querySelector("#mobileNumber").value
	let email = document.querySelector("#userEmail").value
	let password1 = document.querySelector("#password1").value
	let password2 = document.querySelector("#password2").value

	// validate to enable submit button when all fields are populated and both password match
	if((password1 !== '' && password2 !== '') && (password1 === password2) && (mobileNo.length === 11)) {

		//check if email exists
		fetch('https://cess-csp2.herokuapp.com/api/users/email-exists', {
			method: 'POST',
			headers: {
				'Content-type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			if (data === false) {
				fetch('https://cess-csp2.herokuapp.com/api/users', {
					method: 'POST',
					headers: {
					'Content-type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						password: password1,
						mobileNo: mobileNo
					})
				})
				.then(res => res.json())
				.then(data => {

					if (data === true) {
						alert("Successfully registered")
						window.location.replace("./login.html") //redirect sa login page
					}else{
						alert("Registration Failed")
					}
				})
			}
		})

	}else{
		alert("Somethong went wrong. Please re-enter your details")
	}
})