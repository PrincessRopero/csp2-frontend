let token = localStorage.getItem("token");

let profileContainer = document.querySelector('#profileContainer');

if (!token || token === null) {
	alert("You must log in first")
	window.location.replace('./login.html')
}else{
	fetch('https://cess-csp2.herokuapp.com/api/users/details', {
		headers: {
			'Authorization': `Bearer ${token}`
		}
	})
	.then(res => res.json())
	.then(data => {
		// console.log(data)
		profileContainer.innerHTML = 
		`
			<div class="col-md-12">
				<section class="jumbotron my-5">	

					<h3 class="text-center">First Name: <strong>${data.firstName}</strong></h3>
					<h3 class="text-center">Last Name: <strong>${data.lastName}</strong></h3>
					<h3 class="text-center">Email: <strong>${data.email}</strong></h3>
					<h3 class="text-center">Contact Number: <strong>${data.mobileNo}</strong></h3>
					<h3 class="mt-5"><strong>Class History</strong></h3>
					<table class="table">
						<thead>
							<tr>
								<th> Course ID </th>
								<th> Enrolled On </th>
								<th> Status </th>
							</tr>
						</thead>
						<tbody id="coursesContainer"></tbody>
					</table> 

				</section>
			</div>
		`

		let coursesContainer = document.querySelector("#coursesContainer");

		data.enrollments.map(course => {
			fetch(`https://cess-csp2.herokuapp.com/api/courses/${course.courseId}`)
			.then(res => res.json())
			.then(data => {
				// console.log(data)

				coursesContainer.innerHTML += 
				`
					<tr>
						<td>${data.name}</td>
						<td>${course.enrolledOn}</td>
						<td>${course.status}</td>
					</tr>
				`
			})
		})
	})
}