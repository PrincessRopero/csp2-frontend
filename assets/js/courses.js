let adminUser = localStorage.getItem("isAdmin") //localStorage (specific) - naka store ang cookies 
let modalButton = document.querySelector("#adminButton")
let cardFooter;
let token = localStorage.getItem("token")
// conditional rendering
if(adminUser === "false" || !adminUser){
	modalButton.innerHTML = null
}else{
	modalButton.innerHTML = 
	`
	<div class="col-md-2 offset-md-10">
		<a href="./addCourse.html" class="btn-addCourse btn-block btn-primary">Add Course</a>
	</div>
	`
}

// Fetch the course from our API

fetch('https://cess-csp2.herokuapp.com/api/courses/all',{
	headers: {
		'Authorization': `Bearer ${token}`
	}
})
.then(res =>res.json())
.then(data => {
	console.log(data)
	let courseData;
	if(data.length < 1){
		console.log(data.length)
		courseData = "No courses available"
	
	}else{
		if(adminUser === "true" || adminUser === true){
			courseData = data.map(course => {
				//console.log(course.isAdmin)
				if(course.isActive === true){
					cardFooter =
					`
						<a href="./course.html?courseId=${course._id}" class="btn text-white btn-block editButton">Select Course</a>
						<a href="./editCourse.html?courseId=${course._id}" class="btn text-white btn-block editButton">Edit</a>
						<a href="./deleteCourse.html?courseId=${course._id}" class="btn text-white btn-block editButton">Disable Course</a>
					`
				} else{
					cardFooter =
					`
						<a href="./course.html?courseId=${course._id}" class="btn text-white btn-block editButton">Select Course</a>
						<a href="./editCourse.html?courseId=${course._id}" class="btn text-white btn-block editButton">Edit</a>
						<a href="./reenableCourse.html?courseId=${course._id}" class="btn text-white btn-block editButton">Re-enable Course</a>
					`
				}

				return(
					`
						<div class="col-md-6 my-3">
								<div class="card">
									<div class="card-body">
										<h5 class="card-title">${course.name}</h5>
										<p class="card-text text-left">
											${course.description}
										</p>
										<p class="card-text text-right">
											${course.price}
										</p>
									</div>
									
									<div class="card-footer">
										${cardFooter}
									</div>
								</div>
							</div>
					`
					)
			}).join("")
		}else{
			fetch('https://cess-csp2.herokuapp.com/api/courses').then(res => res.json()).then(data => {
				// console.log(data)
				courseData = data.map(course => {
					cardFooter =
						`
							<a href="./course.html?courseId=${course._id}" class="btn btn-primary text-white btn-block editButton">Select Course</a>
						`
						
					return(
						`
							<div class="col-md-6 my-3">
								<div class="card">
									<div class="card-body">
										<h5 class="card-title">${course.name}</h5>
										<p class="card-text text-left">
											${course.description}
										</p>
										<p class="card-text text-right">
											${course.price}
										</p>
									</div>
									
									<div class="card-footer">
										${cardFooter}
									</div>
								</div>
							</div>
						`
						)
				}).join("")
				let container = document.querySelector("#coursesContainer")

				container.innerHTML = courseData;
			})
		}

	}
	let container = document.querySelector("#coursesContainer")

	container.innerHTML = courseData
})